var path = window.location.pathname;

if (path.indexOf('youtrack/issues') != -1) {
    console.log('issues bro');
    showIssueSums();
}

if (path.indexOf('rest/agile/') != -1) {
    console.log('agile bro');
    showAgileSums();
}


/*
    Showing SUMS for Issues
 */

function showIssueSums (){

    var values = $("div[tp7 = '_82-2period'][class *= 'cf-wrapper'][title *= 'Value']");
    var valuesSum = 0;

    var estimates = $("div[tp7 = '_82-0period'][class = 'cf-wrapper']");
    var estimatesSum = 0;

    var done = $("div[tp7 = '_82-1period'][class *= 'cf-wrapper']");
    var doneSum = 0;

    var storyPoints = $("a[title *= 'Story point']");
    console.log(storyPoints);
    var storyPointsSum = 0;

    var regexp = /([\d]+[A-Za-z])/g;


    // VALUES FOR CLIENT ===========
    valuesSum = countHoursSum(valuesSum, values);

    // ESTIMATES ===================
    estimatesSum = countHoursSum(estimatesSum, estimates);

    // DONE ========================
    doneSum = countHoursSum(doneSum, done);

    // STORY POINTS ================
    storyPointsSum = countHoursSum(storyPointsSum, storyPoints);

    // Leftovers ===================
    var leftSum = estimatesSum - doneSum;

    var html = '<div style=\"width:100%; height: 40px; text-align: center\; line-height: 40px; display: inline-block">';

    if (estimatesSum > 0) html += '<b style=\"color: darkred;\">E:' + estimatesSum + '</b> ';
    if (doneSum > 0) html += '<b style=\"color: darkgreen;\">D:' + doneSum + '</b> ';
    if (leftSum > 0) html += '<b style=\"color: darkorange;\">L:' + leftSum + '</b> // ';
    if (storyPointsSum > 0) html += '<b>SP:' + storyPointsSum + '</b> ';
    html += '<b>V:' + valuesSum + '</b></div>';

    $(html).insertAfter('#topToolbarWrapper');

}


/*
    Showing SUMS for Agile
 */

function showAgileSums() {
    var regexp = /([\d]+[A-Za-z])/g;

    var estimations = $('.sb-board-estimation').text().match(regexp);
    var estimationsSum = 0;

    var leftOvers = $('.sb-board-estimation > span').attr('title').split(' ', 1)[0].match(regexp);
    var leftSum = 0;


    // ESTIMATES ===========
    estimationsSum = countHoursSum(estimationsSum, estimations);


    // LEFTOVERS ===========
    leftSum = countHoursSum(leftSum, leftOvers);

    // DONE ================
    var doneSum = estimationsSum - leftSum;


    var html = '<div style=\"width:100%; height: 40px; text-align: center\; line-height: 40px; display: inline-block">';
    if (estimationsSum > 0) html += '<b style=\"color: darkred;\">E:' + estimationsSum + '</b> ';
    if (doneSum > 0) html += '<b style=\"color: darkgreen;\">D:' + doneSum + '</b> ';
    if (leftSum > 0)html += '<b style=\"color: darkorange;\">L:' + leftSum + '</b>';
    html += '</div>';

    $(html).insertAfter('.sb-board-estimation');

}


/*
    Counting hours
 */

function countHoursSum (sum, data) {
    var regexp = /([\d]+[A-Za-z])/g;

    if (data.selector){ //We need to check is it JQUERY object or not
        console.log ('JQuery object');

        for (i = 0; i < data.length; i++) {
            if ($(data[i]).text().match(regexp) != null) var dateElements = $(data[i]).text().match(regexp);
            else  var dateElements = $(data[i]).text();

            countSum(dateElements);
        }

    } else {
        console.log ('other');
        countSum(data);
    }

    return sum;


    /*
        Function is looping through array [1w,1d,1h] and counting real hours
        1w = 40hrs
        1d = 8hrs
        1h = .... guess!
     */

    function countSum (dateElements) {

        for (key in dateElements) {
            var dateElement = dateElements[key];
            console.log(dateElement);

            if (dateElement.indexOf('h') != -1) {
                console.log('hours');
                sum += parseInt(dateElement.substr(0, dateElement.length - 1));
            } else if (dateElement.indexOf('d') != -1) {
                console.log('days');
                sum += parseInt(dateElement.substr(0, dateElement.length - 1)) * 8;
            } else if (dateElement.indexOf('w') != -1) {
                console.log('weeks');
                sum += parseInt(dateElement.substr(0, dateElement.length - 1)) * 40;
            } else {
                console.log('just a number');
                sum += parseInt(dateElement) || 0;
            }

            console.log(sum);
        }
    }

}