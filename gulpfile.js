/* Require node modules
 -------------------------------------------------------------------------------------- */

var gulp       = require('gulp');
var $          = require('gulp-load-plugins')();
var stripDebug = require('gulp-strip-debug');


/* Build vendor scripts
 -------------------------------------------------------------------------------------- */

gulp.task('vendor', function() {

    gulp.src([
        'src/libs/**/*.js'
    ])
        .pipe($.concat('vendor.min.js'))
        .on('error', onError)
        //.pipe($.uglify())
        //.on('error', onError)
        .pipe(gulp.dest('extension/scripts'))

});

/* Build app scripts
 -------------------------------------------------------------------------------------- */

gulp.task('app', function() {

    gulp.src([
        'src/app/init.js',
        'src/app/**/*.js'
    ])

        // Сжатие
        .pipe($.uglify())
        .on('error', onError)

        //Strip debug
        .pipe(stripDebug())
        .on('error', onError)

        // Объединение файлов
        .pipe($.concat('app.min.js'))
        .on('error', onError)

        .pipe(gulp.dest('extension/scripts'))

});

/*
gulp.task('stylus', function() {

    gulp.src(['src/assets/stylus/main.styl'])
        .pipe($.stylus({
            use: [koutoSwiss()]
        }))
        .on('error', onError)
        .pipe(gulp.dest('public/'));
});
*/

gulp.task('zip', function () {
    return gulp.src('extension/**/*')
        .pipe($.zip('extension.zip'))
        .pipe(gulp.dest('./'));
});


/* Watch
 -------------------------------------------------------------------------------------- */

gulp.task('watch', ['vendor', 'app', 'zip'], function() {

    gulp.watch('src/libs/**/*', ['vendor']);

    gulp.watch('src/app/**/*', ['app']);

    gulp.watch('extension/**/*', ['zip']);

    //gulp.watch('src/assets/stylus/**/*', ['stylus']);


});

/* Error handling
 -------------------------------------------------------------------------------------- */

var onError = function(error) {

    $.util.beep();
    console.error('File: ' + error.fileName);
    console.error('Line: ' + error.lineNumber);
    console.error('Message: ' + error.message);

};